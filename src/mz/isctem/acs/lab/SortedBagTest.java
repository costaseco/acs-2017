package mz.isctem.acs.lab;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by jrcs on 06/07/2017.
 */
public class SortedBagTest {

    static final char[] cs = {'A','B','B','B','C','C','C','C','D'};
    private SortedBag<Character> bag;

    @Before
    public void setUp() {
        bag = new SortedBag<Character>();

        for( char c : cs)
            bag.add(c);
    }

    @Test
    public void testAdd() {
        assertTrue(bag.count('A') == 1);
        assertTrue(bag.count('B') == 3);
        assertTrue(bag.count('C') == 4);
        assertTrue(bag.count('D') == 1);
    }

    @Test
    public void testIterator() {
        int i = 0;
        for(char c : bag) {
            assert( i < cs.length);
            assertTrue( c == cs[i++]);
        }
    }

    @Test
    public void testAdd2() {
        assertTrue(bag !=null); //Garantir que a lista ja existe //Precondicao
        for(int i=0;i<50;i++)
        {	bag.add('F');
            bag.add('G');
            bag.add('H');
            assertTrue(bag.count('F') == i+1); //PosCondicao
            assertTrue(bag.count('G') == i+1); //PosCondicao
            assertTrue(bag.count('H') == i+1); //PosCondicao
        }
    }


    @Test(expected = AssertionError.class)
    public void testRemove2() {
        bag.remove('Z');
    }

    @Test
    public void testRemove() {
        assertTrue(bag !=null); //Garantir que a lista ja existe //Precondicao
        for(int i=0;i<50;i++) {
            bag.add('F');
            bag.add('G');
            bag.add('H');
        }
        for(int i=0;i<50;i++)
        {	bag.remove('F');
            bag.remove('G');
            bag.remove('H');
            assertTrue(bag.count('F') == 50-i-1); //PosCondicao
            assertTrue(bag.count('G') == 50-i-1); //PosCondicao
            assertTrue(bag.count('H') == 50-i-1); //PosCondicao
        }
    }

}
