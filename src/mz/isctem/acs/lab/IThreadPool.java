package mz.isctem.acs.lab;

/**
 * Created by jrcs on 04/07/2017.
 */
public interface IThreadPool {
    Thread getNewThread(Runnable r);
    void releaseThread(Thread t);
}
