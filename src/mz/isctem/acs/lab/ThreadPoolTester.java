package mz.isctem.acs.lab;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by jrcs on 04/07/2017.
 */
public class ThreadPoolTester {

    IThreadPool pool;
    private class Counter {
        int count = 0;
        synchronized void inc() { count++; }
        int get() { return count; }
    }

    @Before
    public void setUp() {
        pool = ThreadPool.getInstance();
    }

    @Test
    public void testThreads() {
        final Counter c = new Counter();
        int i = 0;
        final int LIMIT = 100000;
        while( i < LIMIT ) {
            final int value = i;
            Thread t = pool.getNewThread(new Runnable(){
                @Override
                public void run() {
                    c.inc();
                }
            });
            if( t == null ) continue;
            t.run();
            pool.releaseThread(t); // should be done inside run
            i++;
        }
        assertTrue( c.get() == LIMIT );
    }

}
