package mz.isctem.acs.lab;

import java.util.Iterator;
import java.util.TreeMap;

public class SortedBag<T extends Comparable<T>> implements ISortedBag<T>, Iterable<T> {
	
	TreeMap<T,Integer> elements;
	int size;

	public SortedBag() {
		super();
		this.elements = new TreeMap<>();
		this.size = 0;
	}

	// requires true
	// ensures count(elem) == \old(count(elem)) + 1
	@Override
	public void add(T elem) {
		if( elements.containsKey(elem) ) {
			int number = elements.get(elem);
			elements.put(elem, number + 1 );
		}
		else 
			elements.put(elem, 1 );
		size++;
	}

	// requires count(elem) > 0
	// ensures count(elem) == \old(count(elem)) - 1
	@Override
	public void remove(T elem) {
		if( elements.containsKey(elem) ) {
			int number = elements.get(elem);
			if(number == 0)
				elements.remove(elem);
			else if (number > 0)
				elements.put(elem, number -1 );
			else 
				assert(false); // illegal code
			
			size--;
		}
		else
			assert(false); // precondition
			//throw new IllegalArgumentException();
	}

	@Override
	public int count(T elem) {
		return elements.get(elem);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Iterator<T> iterator() {
		final Iterator<T> keys = elements.keySet().iterator();


		return new Iterator<T>() {

			T element = null;
			int count = 0;


			@Override
			public boolean hasNext() {
				refresh();
				return count > 0;
			}

			private void refresh() {
				if ( count == 0 ) {
					if( keys.hasNext() ) {
						element = keys.next();
						count = elements.get(element);
					}
				}
			}

			@Override
			public T next() {
				refresh();
				assert( element != null && count > 0 );
				T current = element;
				if( --count == 0 )
					element = null;
				return current;
			}
		};
	}

	@Override
	public void unique() {
		for( T key: elements.keySet())
			elements.put(key,1);
	}

	@Override
	public T max() {
		return elements.lastKey();
	}

	@Override
	public T min() {
		return elements.firstKey();
	}

}
