package mz.isctem.acs.lab;

import java.util.Iterator;

public interface ISortedBag<T extends Comparable<T>> extends IBag<T> {
	void add(T elem);
	
	// requires true
	// ensures if count(elem) > 0 then count(elem) = \old(count(elem)) - 1
	//         else throws IllegalArgumentException
	void remove(T elem) throws IllegalArgumentException;
	
	int count(T elem);
	int size();
	Iterator<T> iterator();
	void unique();
	T max();
	T min();
}
