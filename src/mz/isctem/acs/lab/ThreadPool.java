package mz.isctem.acs.lab;

/**
 * Created by jrcs on 03/07/2017.
 */
public class ThreadPool implements IThreadPool {

    // Singleton pattern

    private static final int MAX_THREADS = 10;
    private static IThreadPool instance = new ThreadPool(MAX_THREADS);

    public static IThreadPool getInstance() { return instance; }

    // ThreadPool implementation

    private final int maxThreads;
    private Thread[] pool ;
    private Boolean[] busy ;

    private ThreadPool(int max) {
        this.maxThreads = max;
        this.pool = new Thread[maxThreads];
        this.busy = new Boolean[maxThreads];
        for(int i = 0; i < maxThreads; i++) {
            this.busy[i] = false;
            this.pool[i] = null;
        }
    }

    synchronized public Thread getNewThread(Runnable r) {
        for(int i = 0; i < maxThreads; i++) {
            if (!busy[i]) {
                busy[i] = true;
                return pool[i] = new Thread(r);
            }
        }
        return null;
    }

    synchronized public void releaseThread(Thread t) {
        for(int i = 0; i < maxThreads; i++) {
            if (pool[i] == t) {
                busy[i] = false;
                pool[i] = null;
            }
        }
    }
}
