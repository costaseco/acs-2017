package mz.isctem.acs.lab;

import static org.junit.Assert.*;

import org.junit.Test;

import java.lang.*;

public class MathTest {

	@Test
	public void test() {
		int[] a= {0,20,10,2,3,5};
		assertTrue(new Math().max(a) == 20);
	}

	@Test
	public void testFact() {
		assertTrue(new Math().fact(0) == 1);
		assertTrue(new Math().fact(1) == 1);
		assertTrue(new Math().fact(3) == 6);
		assertTrue(new Math().fact(10) == 3628800);
	}	
	
	@Test
	public void testFactFull() {
		assertTrue(new Math().factFull(0) == 1);
		assertTrue(new Math().factFull(1) == 1);
		assertTrue(new Math().factFull(3) == 6);
		assertTrue(new Math().factFull(10) == 3628800);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFactFullX() {
		assertTrue(new Math().factFull(-1) == 0);
	}
	
	
}
