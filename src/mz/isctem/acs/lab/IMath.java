package mz.isctem.acs.lab;

public interface IMath {
	// requires a.length > 0
	// ensures returns the maximum value of the array
	int max(int[] a);
	
	// requires n >= 0
	// ensures the result is n!
	int fact(int n);

	// requires true
	// ensures if n >= 0 then result is n!
	// ensures if n < 0 then throws InvalidArgumentException
	int factFull(int n);
}
