package mz.isctem.acs.lab;

import java.util.ArrayList;
import java.util.Iterator;

public class Bag<T> implements IBag<T> {
	
	ArrayList<T> elements;

	public Bag() {
		super();
		this.elements = new ArrayList<>();
	}

	@Override
	public void add(T elem) {
		elements.add(elem);
	}

	@Override
	public void remove(T elem) {
		assert( elements.remove(elem) ); // requires the existence
	}

	@Override
	public int count(T elem) {
		return elements.stream().filter(x -> x.equals(elem)).toArray().length;
	}

	@Override
	public int size() {
		return elements.size();
	}

	@Override
	public Iterator<T> iterator() {
		return elements.iterator();
	}

}
