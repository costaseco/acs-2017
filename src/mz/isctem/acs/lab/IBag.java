package mz.isctem.acs.lab;

import java.util.Iterator;

public interface IBag<T> {
	void add(T elem);
	// requires count(elem) > 0
	// ensures count(elem) = \old(count(elem)) - 1
	void remove(T elem);
	int count(T elem);
	int size();
	Iterator<T> iterator();
}
