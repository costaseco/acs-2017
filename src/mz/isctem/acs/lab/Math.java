package mz.isctem.acs.lab;

class Math implements IMath {
	
	@Override
	public int max(int[] a) {
		int m = a[0];
		for(int x : a)
			if( m < x ) m = x;
		return m;
	}

	@Override
	public int fact(int n) {
		if (n == 0) 
			return 1;
		else 
			return n * fact(n-1);
	}


	@Override
	public int factFull(int n) 
			throws IllegalArgumentException {

		if (n < 0 ) throw new IllegalArgumentException();

		if (n == 0) 
			return 1;
		else 
			return n * fact(n-1);
	}
}
