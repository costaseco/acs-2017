package mz.isctem.acs.patterns;

/**
 * Created by jrcs on 04/07/2017.
 */
public interface IObserver<T,S> {
    S update(T subject);
}
