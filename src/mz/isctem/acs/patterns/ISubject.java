package mz.isctem.acs.patterns;

import mz.isctem.acs.patterns.IObserver;

/**
 * Created by jrcs on 04/07/2017.
 */
public interface ISubject<T,S> {
    void subscribe(IObserver<T,S> observer);
    void unsubscribe(IObserver<T,S> observer);
    S notifyObservers();
}
