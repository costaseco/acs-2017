package mz.isctem.acs.taskManager;

import mz.isctem.acs.taskManager.model.IPerson;
import mz.isctem.acs.patterns.ISubject;
import mz.isctem.acs.taskManager.model.ITask;

import java.util.Collection;
import java.util.Date;

/**
 * Created by jrcs on 04/07/2017.
 */
public interface IApplication {

    IPerson addPerson(String name, Date birthdate, String email, String title);

    IPerson searchPerson(String name);

    ITask addTask(String description, Date startDate, Date dueDate);

    ITask addSprint(String description, Date startDate, Date dueDate, Collection<ISubject> tasks);

    ITask searchTask(int number);

    void assignTask(ITask task, IPerson person);

    void unlinkTask(ITask task, IPerson person);


//    void assignTask();
//    void createTeam();
//    void lateTasks();
//    void projectProgress();
//    void createSprint();
//    void createProject();
//    void closeProject();
//    void moveTask();
//    void closeSprint();
}
