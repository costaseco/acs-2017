package mz.isctem.acs.taskManager.services;

import mz.isctem.acs.taskManager.model.IPerson;
import mz.isctem.acs.taskManager.model.IPersonCollection;
import mz.isctem.acs.taskManager.model.PersonCollection;

import java.util.Date;

public class PersonService {

    private IPersonCollection persons;

    public PersonService() {
        persons = PersonCollection.getInstance();
        assert(invOk());
    }

    public IPerson createPerson(String name, Date birthdate, String email, String title) {
        return persons.createPerson(name, birthdate, email, title);
    }

    public IPerson searchPerson(String name) {
        return persons.searchPerson(name);
    }

    public boolean invOk() {
        return persons != null;
    }

    public void addPerson(IPerson person) {
        persons.add(person);
    }
}
