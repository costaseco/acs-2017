package mz.isctem.acs.taskManager.services;

import mz.isctem.acs.patterns.ISubject;
import mz.isctem.acs.taskManager.model.ITask;
import mz.isctem.acs.taskManager.model.ITaskCollection;
import mz.isctem.acs.taskManager.model.TaskCollection;

import java.util.Date;

public class TaskService {

    private ITaskCollection tasks;

    public TaskService() {
        this.tasks = TaskCollection.getInstance();
        assert(invOk());
    }

    public ITask createTask(String description, Date startDate, Date dueDate) {
        return tasks.createTask(description, startDate, dueDate);
    }

    public ITask searchTask(String name) {
        return tasks.searchTask(name);
    }

    public boolean invOk() {
        return tasks != null;
    }
}
