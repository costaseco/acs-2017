package mz.isctem.acs.taskManager.tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import mz.isctem.acs.taskManager.model.IPerson;
import mz.isctem.acs.taskManager.model.IPersonCollection;
import mz.isctem.acs.taskManager.model.PersonCollection;
import org.junit.Before;
import org.junit.Test;

public class PersonCollectionTester {

	private static final String PERSONS_CSV = "files/persons.csv";
	private IPersonCollection col;

	@Before
	public void setUp() {
		
		col = PersonCollection.getInstance();
		
		int errors;
		try {
			errors = col.loadPersons(PERSONS_CSV);
			assertTrue(errors == 0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void test() {
		String name = "Sydney Oneil";
		IPerson p = col.searchPerson(name);
		assertTrue(p != null);
		assertTrue(p.getName().equals(name));
	}
	
	@Test
	public void testNotFound() {
		IPerson p = col.searchPerson("Not a name");
		assertTrue(p == null);
	}
}








