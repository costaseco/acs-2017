package mz.isctem.acs.taskManager.tests;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mz.isctem.acs.patterns.ISubject;
import mz.isctem.acs.taskManager.model.*;
import org.junit.Before;
import org.junit.Test;

public class TaskCollectionTester {
	
	Task testedTask;
	static final String DESCRIPTION = "Bla";

	@Before
	public void buildTask() {
		testedTask = new Task();
		testedTask.setNumber(1);
		testedTask.setDescription(DESCRIPTION);
		try {
			testedTask.setStartDate(
					new SimpleDateFormat("dd/MM/yyyy")
					.parse("29/6/2017"));
			testedTask.setDueDate(
					new SimpleDateFormat("dd/MM/yyyy")
					.parse("01/7/2017"));
//			System.out.println(testedTask.getStartDate());
//			System.out.println(testedTask.getDueDate());
//			System.out.println(testedTask.getStartDate().before(testedTask.getDueDate()));
		} catch (ParseException e) {
//			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testTask() {		
		assertTrue(testedTask.invOk());
		assertTrue(testedTask.getDescription().equals(DESCRIPTION));
	}
	
	@Test
	public void testPriorityContract() {
		testedTask.setPriority(5);
	}

	@Test(expected = AssertionError.class)
	public void testPriorityContractX() {
		// contract violation
		testedTask.setPriority(0);
	}

	@Test
	public void testCompleted() {
		try {
			testedTask.completeTask(new SimpleDateFormat("dd/MM/yyyy")
                    .parse("02/07/2017"));
			assertTrue(testedTask.isComplete());
			assertTrue(testedTask.isLate());

			testedTask.completeTask(new SimpleDateFormat("dd/MM/yyyy")
					.parse("01/07/2017"));
			assertTrue(testedTask.isComplete());
			assertFalse(testedTask.isLate());
		} catch (ParseException e) {
			e.printStackTrace();
			fail();
		}
	}

	@Test
    public void testCollection() {
        ITaskCollection factory = TaskCollection.getInstance();

        try {
            Date d1 = new SimpleDateFormat("dd/MM/yyyy")
                    .parse("01/07/2017");
            Date d2 = new SimpleDateFormat("dd/MM/yyyy")
                    .parse("02/07/2017");
            ISubject t = factory.createTask("bla", d1, d2);
            assertTrue(((Task) t).invOk());

        } catch (ParseException e) {
            e.printStackTrace();
            fail();
        }

    }
}


