package mz.isctem.acs.taskManager.tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import mz.isctem.acs.taskManager.model.TeamCollection;
import mz.isctem.acs.taskManager.model.*;
import org.junit.Before;
import org.junit.Test;

public class TeamCollectionTester {

    private static final String PERSONS_CSV = "files/persons.csv";
    private IPersonCollection persons;
    private ITeamCollection teams;

    @Before
    public void setUp() {

        teams = TeamCollection.getInstance();
        persons = PersonCollection.getInstance();

        int errors;
        try {
            errors = persons.loadPersons(PERSONS_CSV);
            assertTrue(errors == 0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            fail();
        }

        String[] teamNames = {"Swat", "Cleaning", "Terrain", "Air"};
        String[][] members =
                {{"Sydney Oneil", "Kameko Moore"},
                        {"Amy Brennan","Shellie Washington"},
                        {"Rahim Blankenship"},
                        {"Yvonne Lopez", "Steel Price"}};

        int i = 0;
        for( String name: teamNames ) {
            ITeam team = teams.createTeam(name);
            for( String personName : members[i] ) {
                IPerson person = persons.searchPerson(personName);
                team.getMembers().add(person);
                person.setTeam(team);
            }
            teams.add(team);
            i++;
        }
    }

    @Test
    public void collectionTest() {
        assertTrue(teams.size() == 4);
    }

    @Test
    public void test() {
        String name = "Swat";
        ITeam team = teams.searchTeam(name);
        assertTrue(team != null);
        assertTrue(team.getName().equals(name));
        assertTrue(team.getMembers().size() == 2);
    }

    @Test
    public void testMembers1() {
        String teamName = "Swat";
        String name = "Sydney Oneil";
        IPerson person = persons.searchPerson(name);
        ITeam team = teams.searchTeam(teamName);
        assertTrue( team != null );
        assertTrue( person != null );
        assertTrue(person.getTeam() == team);
        assertTrue( team.getMembers().contains(person));
    }

    @Test
    public void testMembers2() {
        String teamName = "Terrain";
        String name = "Rahim Blankenship";
        IPerson person = persons.searchPerson(name);
        ITeam team = teams.searchTeam(teamName);
        assertTrue( team != null );
        assertTrue( person != null );
        assertTrue(person.getTeam() == team);
        assertTrue( team.getMembers().contains(person));
    }


    @Test
    public void testNotFound() {
        ITeam p = teams.searchTeam("Not a name");
        assertTrue(p == null);
    }
}








