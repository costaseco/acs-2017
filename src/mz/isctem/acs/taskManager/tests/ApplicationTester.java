package mz.isctem.acs.taskManager.tests;

import mz.isctem.acs.taskManager.Application;
import mz.isctem.acs.taskManager.model.IPerson;
import mz.isctem.acs.taskManager.services.PersonService;
import mz.isctem.acs.taskManager.services.TaskService;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertTrue;

/**
 * Created by jrcs on 04/07/2017.
 */
public class ApplicationTester {
    private static Application app;
    private IPerson person;
    private Date birthdate;


    @BeforeClass
    public static void setupAll() {
        PersonService persons = new PersonService();
        TaskService tasks = new TaskService();

        app = new Application();
        app.setPersons(persons);
        app.setTasks(tasks);
    }

    @Test
    public void testApplicationAssembly() {
        assert(app.invOk());
    }

    @Test
    public void testAddPerson() {
        try {
            birthdate = new SimpleDateFormat("dd/MM/yyyy")
                    .parse("28/10/1981");
            person = app.addPerson("Abel Iapa",
                    birthdate,
                    "abel@google.com",
                    "developer"
            );
            assertTrue(person.getName().equals("Abel Iapa"));
            assertTrue(person.getBirthdate().equals(birthdate));
            assertTrue(person.getTitle().equals("developer"));
            IPerson p = app.searchPerson("Abel Iapa");
            assertTrue(p == person);
        } catch (ParseException e) {
            e.printStackTrace();
            fail();
        }
    }

}
