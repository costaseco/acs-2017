package mz.isctem.acs.taskManager.model;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TeamCollection implements ITeamCollection {

    // Singleton pattern

    private static ITeamCollection instance = new TeamCollection();

    public static ITeamCollection getInstance() { return instance; }

    // The collection fields and methods

    private Map<String, ITeam> collection;

    private TeamCollection() {
        clear();
    }

    @Override
    public ITeam createTeam(String name) {
        Team team = new Team();
        team.setName(name);
        team.setMembers(new ArrayList<>());
        team.setTasks(new ArrayList<>());
        assert(team.invOk());
        return team;
    }

    @Override
    public int loadTeams(IPersonCollection persons) throws FileNotFoundException {
        // Code removed to the test
        return 0;
    }

    @Override
    public int size() {
        return collection.size();
    }

    @Override
    public boolean isEmpty() {
        return collection.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return collection.containsValue(o);
    }

    @Override
    public Iterator<ITeam> iterator() {
        return collection.values().iterator();
    }

    @Override
    public Object[] toArray() {
        return collection.values().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return collection.values().toArray(a);
    }

    @Override
    public boolean add(ITeam p) {
        collection.put(p.getName(), p);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        assert( o instanceof Team );
        return collection.remove(((Team)o).getName(), o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return collection.values().containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends ITeam> c) {
        for(ITeam p:c) add(p);
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for(Object o:c) changed |= remove(o);
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {

        boolean changed = false;
        for(ITeam p:collection.values())
            if(!c.contains(p))
                changed |= null != collection.remove(p);
        return changed;
    }

    @Override
    public void clear() {
        collection = new HashMap<>();
    }

    @Override
    public ITeam searchTeam(String name) {
        return collection.get(name);
    }
}
