package mz.isctem.acs.taskManager.model;

import mz.isctem.acs.patterns.ISubject;

import java.util.Date;
import java.util.Iterator;

public interface IPerson {

    // ensures \result != null
    String getName();

    // ensures \result != null
    Date getBirthdate();

    // ensures \result != null
    String getEmail();

    // ensures \result != null
    String getTitle();

    // ensures \result != null
    Iterator<ITask> taskIterator();

	ITeam getTeam();
    void setTeam(ITeam team);

    void assignTask(ITask task);
    boolean removeTask(ITask task);
}
