package mz.isctem.acs.taskManager.model;

import mz.isctem.acs.patterns.ISubject;

import java.util.Date;

public interface ITask extends ISubject<ITask,Boolean> {
	String getDescription();
	Date getStartDate();
	
	Date getDueDate();	
	
	// requires days >= 0
	void postpone(int days);
	
	Date getDeliveryDate();
	void completeTask(Date date);
	boolean isLate();
	
	// ensures 1 <= \result && \result <= 10
	int getPriority();

	// requires 1 <= priority && priority <= 10
	void setPriority(int priority);
	
	TaskEffort getEffort();
	void setEffort(TaskEffort effort);

    void assignPerson(IPerson person);
    boolean removePerson(IPerson person);

	boolean isComplete();


}
