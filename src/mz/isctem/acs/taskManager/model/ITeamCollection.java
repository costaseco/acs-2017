package mz.isctem.acs.taskManager.model;

import java.io.FileNotFoundException;
import java.util.Collection;

/**
 * Created by jrcs on 01/07/2017.
 */
public interface ITeamCollection extends Collection<ITeam> {
    ITeam createTeam(String name);

    int loadTeams(IPersonCollection persons) throws FileNotFoundException;

    ITeam searchTeam(String name);
}
