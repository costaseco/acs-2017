package mz.isctem.acs.taskManager.model;

import mz.isctem.acs.patterns.ISubject;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class Person implements IPerson {

	private String name;
	private Date birthdate;
	private String email;
	private String title;
	private Collection<ITask> tasks;
	private ITeam team;

	//== Bean methods

	public Person() {}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Collection<ITask> getTasks() {
		return tasks;
	}

	public void setTasks(Collection<ITask> tasks) {
		this.tasks = tasks;
	}

	@Override
	public Iterator<ITask> taskIterator() {
		return tasks.iterator();
	}

    @Override
    public ITeam getTeam() {
        return team;
    }

    @Override
    public void setTeam(ITeam team) {
        this.team = team;
    }

	@Override
	public void assignTask(ITask task) {
		tasks.add(task);
	}

	@Override
	public boolean removeTask(ITask task) {
		return tasks.remove(task);
	}


	//==== My Methods

    public Person(String name, Date birthdate, String email, String title) {
        this.name = name;
        this.birthdate = birthdate;
        this.email = email;
        this.title = title;
    }

    public boolean invOk() {
        return name != null &&
                name.length() > 2 &&
                validEmail(email) &&
                birthdate != null &&
                title != null &&
                tasks != null;
    }

    private boolean validEmail(String email) {
	    // This is clearly imcomplete...
	    return email != null && email.contains("@");
    }
}

