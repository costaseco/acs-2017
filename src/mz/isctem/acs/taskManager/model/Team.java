package mz.isctem.acs.taskManager.model;

import mz.isctem.acs.patterns.ISubject;

import java.util.Collection;

/**
 * Created by jrcs on 01/07/2017.
 */
class Team implements ITeam {
    private String name;
    private Collection<IPerson> members;
    private Collection<ISubject> tasks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<IPerson> getMembers() {
        return members;
    }

    public void setMembers(Collection<IPerson> members) {
        this.members = members;
    }

    public Collection<ISubject> getTasks() {
        return tasks;
    }

    public void setTasks(Collection<ISubject> tasks) {
        this.tasks = tasks;
    }

    public boolean invOk() {
        return name != null &&
                members != null &&
                // members.size() > 0 &&
                tasks != null;
    }
}
