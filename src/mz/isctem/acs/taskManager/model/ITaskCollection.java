package mz.isctem.acs.taskManager.model;

import mz.isctem.acs.patterns.ISubject;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Date;

public interface ITaskCollection extends Collection<ISubject> {

    int loadTasks(String filename) throws FileNotFoundException;

    ITask searchTask(String name);

    ITask createTask(String description, Date startDate, Date dueDate);
}
