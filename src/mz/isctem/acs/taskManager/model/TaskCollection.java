package mz.isctem.acs.taskManager.model;

import mz.isctem.acs.patterns.ISubject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class TaskCollection implements ITaskCollection {

    // Singleton pattern

    private static ITaskCollection instance;

    static {
        instance = new TaskCollection();
    }

    public static ITaskCollection getInstance() {
        return instance;
    }

    // The collection fields and methods

    private ArrayList<ISubject> collection;

    private int sequenceNumber = 0;

    private TaskCollection() {
        clear();
    }

    @Override
    public ITask createTask(String description, Date startDate, Date dueDate) {
        Task task = new Task();
        task.setNumber(++sequenceNumber);
        task.setDescription(description);
        task.setStartDate(startDate);
        task.setDueDate(dueDate);
        assert(task.invOk());
        return task;
    }

    @Override
    public int loadTasks(String filename) throws FileNotFoundException {
        return 0;
    }

    @Override
    public int size() {
        return collection.size();
    }

    @Override
    public boolean isEmpty() {
        return collection.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return collection.contains(o);
    }

    @Override
    public Iterator<ISubject> iterator() {
        return collection.iterator();
    }

    @Override
    public Object[] toArray() {
        return collection.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return collection.toArray(a);
    }

    @Override
    public boolean add(ISubject p) {
        return collection.add(p);
    }

    @Override
    public boolean remove(Object o) {
        assert( o instanceof ITask );
        return collection.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return collection.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends ISubject> c) {
        for(ISubject p:c) add(p);
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean changed = false;
        for(Object o:c) changed |= remove(o);
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return collection.retainAll(c);
    }

    @Override
    public void clear() {
        collection = new ArrayList<>();
    }

    @Override
    public ITask searchTask(String name) {
        return null;
    }
}
