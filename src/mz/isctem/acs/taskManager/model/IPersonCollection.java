package mz.isctem.acs.taskManager.model;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Date;

public interface IPersonCollection extends Collection<IPerson> {

	int loadPersons(String filename) throws FileNotFoundException;

	IPerson searchPerson(String name);

	IPerson createPerson(String name, Date date, String email, String title);	
}
