package mz.isctem.acs.taskManager.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class PersonCollection implements IPersonCollection {

	// Singleton pattern

	private static IPersonCollection instance;

	static {
		instance = new PersonCollection();
	}

	public static IPersonCollection getInstance() { return instance; }

	private PersonCollection() {
		clear();
	}

	// The collection fields and methods

	private Map<String, IPerson> collection;

	@Override
	public IPerson createPerson(String name, Date date, String email, String title) {
		Person person = new Person();
		person.setName(name);
		person.setBirthdate(date);
		person.setEmail(email);
		person.setTitle(title);
		person.setTasks(new ArrayList<>());
		assert(person.invOk());
		return person;
	}

	public int loadPersons(String filename) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(filename));

		String header = sc.nextLine();

		assert(header.equals("Name;Email;Birthdate"));

		int errors = 0;

		while(sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] components = line.split(";");

			assert(components.length == 3);

			try {
				IPerson p = createPerson(components[0],
						new SimpleDateFormat("dd/MM/yyyy")
								.parse(components[2]),
						components[1],
						"none");
				add(p);
			} catch (ParseException e) {
				errors++;
			}
		}
		sc.close();
		return errors;
	}

	@Override
	public int size() {
		return collection.size();
	}

	@Override
	public boolean isEmpty() {
		return collection.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return collection.containsValue(o);
	}

	@Override
	public Iterator<IPerson> iterator() {
		return collection.values().iterator();
	}

	@Override
	public Object[] toArray() {
		return collection.values().toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return collection.values().toArray(a);
	}

	@Override
	public boolean add(IPerson p) {
		collection.put(p.getName(), p);
		return true;
	}

	@Override
	public boolean remove(Object o) {
		assert( o instanceof Person ); 
		return collection.remove(((Person)o).getName(), o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return collection.values().containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends IPerson> c) {
		for(IPerson p:c) add(p);
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean changed = false;
		for(Object o:c) changed |= remove(o);
		return changed;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		
		boolean changed = false;
		for(IPerson p:collection.values())
			if(!c.contains(p))
				changed |= null != collection.remove(p);
		return changed;
	}

	@Override
	public void clear() {
		collection = new HashMap<>();
	}

	@Override
	public IPerson searchPerson(String name) {
		return collection.get(name);
	}
}
