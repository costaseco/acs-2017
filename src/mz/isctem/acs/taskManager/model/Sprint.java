package mz.isctem.acs.taskManager.model;

import mz.isctem.acs.patterns.IObserver;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by jrcs on 04/07/2017.
 */
public class Sprint implements ITask, IObserver<ITask,Boolean> {

    private String description;
    private int number;
    private Date dueDate;
    private ArrayList<ITask> subtasks;

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public Date getStartDate() {
        // return subtasks.stream().min((o1, o2) -> o1.getStartDate().compareTo(o2.getStartDate())).get().getStartDate();
        return subtasks
                .stream()
                .map(ITask::getStartDate)
                .min(Date::compareTo)
                .get();
    }

    @Override
    public Date getDueDate() {
        return this.dueDate;
    }

    @Override
    public void postpone(int days) {
        assert (days >= 0);
        Calendar c = Calendar.getInstance();
        c.setTime(this.dueDate);
        c.add(Calendar.DATE, days);
        this.dueDate = c.getTime();
    }

    @Override
    public Date getDeliveryDate() {

        Date maxDate = null;
        for(ITask t: subtasks)
            if( t.getDeliveryDate() == null ) return null;
            else if ( maxDate == null ) maxDate = t.getDeliveryDate();
                 else if( maxDate.before(t.getDeliveryDate())) maxDate = t.getDeliveryDate();
        return maxDate;



//        if (subtasks
//                .stream()
//                .allMatch(o -> o.getDeliveryDate() != null))
//            return subtasks
//                    .stream()
//                    .map(ITask::getDeliveryDate)
//                    .max(Date::compareTo)
//                    .get();
//        else
//            return null;
    }

    @Override
    public void completeTask(Date date) {
        for( ITask t: subtasks)
            if( ! t.isComplete() ) t.completeTask(date);
    }

    @Override
    public boolean isLate() {
        return this.getDueDate().before(this.getDeliveryDate());
    }

    @Override
    public int getPriority() {
        return 0;
    }

    @Override
    public void setPriority(int priority) {

    }

    @Override
    public TaskEffort getEffort() {
        return null;
    }

    @Override
    public void setEffort(TaskEffort effort) {

    }

    @Override
    public void assignPerson(IPerson person) {

    }

    @Override
    public boolean removePerson(IPerson person) {
        return false;
    }

    @Override
    public boolean isComplete() {
        return subtasks
                .stream()
                .allMatch(o -> o.getDeliveryDate() != null);
    }

    public boolean invOk() {
        return this.number > 0 &&
                this.description != null &&
                this.description.length() > 0 &&
                this.getStartDate().before(this.dueDate) &&
                subtasks.stream()
                        .allMatch(t -> ((Task)t).invOk() &&
                                ! this.getDueDate().before(t.getDueDate()));
    }


    public void addSubTask(ITask t) {
        subtasks.add(t);
        t.subscribe(this);
    }


    public void removeSubTask(ITask t) {
        subtasks.remove(t);
        t.unsubscribe(this);
    }



    @Override
    public Boolean update(ITask subject) {
        return invOk();
    }

    @Override
    public void subscribe(IObserver<ITask,Boolean> observer) {

    }

    @Override
    public void unsubscribe(IObserver<ITask,Boolean> observer) {

    }

    @Override
    public Boolean notifyObservers() {
        return true;
    }
}
