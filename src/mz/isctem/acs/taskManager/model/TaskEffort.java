package mz.isctem.acs.taskManager.model;

public enum TaskEffort { LOW, MEDIUM, HIGH };
