package mz.isctem.acs.taskManager.model;

import java.util.Collection;

public interface ITeam {
    String getName();
    Collection<IPerson> getMembers();
}
