package mz.isctem.acs.taskManager.model;

import mz.isctem.acs.patterns.IObserver;

import java.util.*;

// enum Status {OPEN, CLOSED, ... };


public class Task implements ITask {

    private int number;
    private String description;
    private Date startDate, dueDate, deliveryDate;
    private int priority;
    // private Status status;
    private TaskEffort effort;

    private Project project;
    private Collection<IPerson> workers;

    //=== Bean Methods

    public Task() {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        Date temp = this.dueDate;
        this.dueDate = dueDate;
        try {
            notifyObservers();
        } catch ( RuntimeException e) {
            this.dueDate = temp;
        }
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public int getPriority() {
        return priority;
    }

    // requires 1 <= priority && priority <= 10
    public void setPriority(int priority) {
        assert (1 <= priority && priority <= 10);
        this.priority = priority;
    }

    public TaskEffort getEffort() {
        return effort;
    }

    public void setEffort(TaskEffort effort) {
        this.effort = effort;
    }

    @Override
    public void assignPerson(IPerson person) {
        workers.add(person);
    }

    @Override
    public boolean removePerson(IPerson person) {
        return workers.remove(person);
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Collection<IPerson> getWorkers() {
        return workers;
    }

    public void setWorkers(Collection<IPerson> workers) {
        this.workers = workers;
    }

    //====== My Methods

    public Iterator<IPerson> workersIterator() { return workers.iterator(); }

    @Override
    // requires days >= 0
    public void postpone(int days) {
        assert (days >= 0);
        Calendar c = Calendar.getInstance();
        c.setTime(this.dueDate);
        c.add(Calendar.DATE, days);
        this.dueDate = c.getTime();
    }

    @Override
    public void completeTask(Date date) {
        this.setDeliveryDate(date);
    }

    public boolean isComplete() {
        return this.getDeliveryDate() != null;
    }

    public boolean invOk() {
        return this.number > 0 &&
                this.description != null &&
                this.description.length() > 0 &&
                this.startDate.before(this.dueDate);
    }

    public boolean isLate() {
        return this.getDueDate().before(this.getDeliveryDate());
    }

    ArrayList<IObserver<ITask,Boolean>> observers = new ArrayList();

    @Override
    public void subscribe(IObserver<ITask,Boolean> observer) {
        observers.add(observer);
    }

    @Override
    public void unsubscribe(IObserver<ITask,Boolean> observer) {
        observers.remove(observer);
    }

    @Override
    public Boolean notifyObservers() {
        for( IObserver<ITask,Boolean> o: observers)
            if( ! o.update(this) ) return false;
        return true;
    }
}
