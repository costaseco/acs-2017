package mz.isctem.acs.taskManager;

import mz.isctem.acs.patterns.ISubject;
import mz.isctem.acs.taskManager.model.*;
import mz.isctem.acs.taskManager.services.PersonService;
import mz.isctem.acs.taskManager.services.TaskService;

import java.util.Collection;
import java.util.Date;

public class Application implements IApplication {
    private PersonService persons;
    private TaskService tasks;

    @Override
    public IPerson addPerson(String name, Date birthdate, String email, String title) {
        IPerson person = persons.createPerson(name, birthdate, email, title);
        persons.addPerson(person);
        return person;
    }

    @Override
    public IPerson searchPerson(String name) {
        return persons.searchPerson(name);
    }

    @Override
    public ITask addTask(String description, Date startDate, Date dueDate) {
        return null;
    }

    @Override
    public ITask addSprint(String description, Date startDate, Date dueDate, Collection<ISubject> tasks) {
        return null;
    }

    @Override
    public ITask searchTask(int number) {
        return null;
    }

    @Override
    public void assignTask(ITask task, IPerson person) {
        task.assignPerson(person);
        person.assignTask(task);
    }

    @Override
    public void unlinkTask(ITask task, IPerson person) {
        task.removePerson(person);
        person.removeTask(task);
    }

    public void setPersons(PersonService persons) {
        this.persons = persons;
    }

    public void setTasks(TaskService tasks) {
        this.tasks = tasks;
    }

    public boolean invOk() {
        return persons != null && tasks != null && persons.invOk() && tasks.invOk();
    }
}
